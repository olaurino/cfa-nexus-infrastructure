import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { ArgoCdApplication, ArgoCdAppArgs } from "../app-platform/argocd/app";

export class StaticWebsite extends pulumi.ComponentResource {
    // public readonly app: ArgoCdApplication;
    public readonly resources;

    constructor(name: string, opts?: pulumi.ComponentResourceOptions) {
        super("cfa-nexus:StaticWebsite", name, {}, opts);

        const config = new pulumi.Config().requireObject<StaticWebsiteConfig>(name);

        this.resources = this.generateManifest(name, config, opts);
        //
        // const argoCDAppArgs: ArgoCdAppArgs = {
        //     name,
        //     namespace: name,
        //     repoUrl: config.repo,
        //     path: config.path,
        //     version: config.revision
        // };
        //
        // this.app = new ArgoCdApplication(name, argoCDAppArgs, { parent: this });

        this.registerOutputs();
    }

    private generateManifest(name: string, swConfig: StaticWebsiteConfig, opts?: pulumi.ComponentResourceOptions) {
        // Set default values and allow overrides through the configuration
        const containerImage = swConfig.containerImage;
        const ingressHost = new pulumi.Config().require("fqdn");
        const ingressClass = swConfig.ingressClass || "nginx";
        const ingressPath = swConfig.ingressPath || "/";
        const servicePort = swConfig.servicePort || 80;

        const namespace = new k8s.core.v1.Namespace(`${name}-ns`, {
            metadata: {
                name: name,
            }
        }, {...opts, parent: this})

        const deployment = new k8s.apps.v1.Deployment(`${name}-deployment`, {
            metadata: {
                name: `${name}-deployment`,
                namespace: namespace.metadata.name,
            },
            spec: {
                replicas: 1,
                selector: {
                    matchLabels: {
                        app: name,
                    },
                },
                template: {
                    metadata: {
                        labels: {
                            app: name,
                        },
                    },
                    spec: {
                        containers: [{
                            name: "nginx",
                            image: containerImage,
                            ports: [{
                                containerPort: servicePort,
                            }],
                            env: [
                                {
                                    name: "BASE_URL",
                                    value: `https://${ingressHost}`,
                                },
                            ],
                        }],
                    },
                },
            },
        }, {...opts, parent: this});

        const service = new k8s.core.v1.Service(`${name}-service`, {
            metadata: {
                name: `${name}-service`,
                namespace: namespace.metadata.name,
            },
            spec: {
                selector: {
                    app: name,
                },
                ports: [{
                    protocol: "TCP",
                    port: servicePort,
                    targetPort: servicePort,
                }],
                type: "ClusterIP",
            },
        }, {...opts, parent: this});

        const ingress = new k8s.networking.v1.Ingress(`${name}-ingress`, {
            apiVersion: "networking.k8s.io/v1",
            kind: "Ingress",
            metadata: {
                name: `${name}-ingress`,
                namespace: namespace.metadata.name,
                annotations: {
                    "kubernetes.io/ingress.class": ingressClass,
                },
            },
            spec: {
                rules: [{
                    host: ingressHost,
                    http: {
                        paths: [{
                            pathType: "ImplementationSpecific",
                            path: ingressPath,
                            backend: {
                                service: {
                                    name: `${name}-service`,
                                    port: {
                                        number: servicePort,
                                    },
                                },
                            },
                        }],
                    },
                }],
            },
        }, {...opts, parent: this});

        return {namespace, deployment, service, ingress};
    }
}

interface StaticWebsiteConfig {
    containerImage: string;
    ingressClass?: string;
    servicePort?: number;

    ingressPath: string;

    repo: string;
    path: string;

    revision: string;
}
