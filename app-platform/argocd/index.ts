import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";
import * as path from "path";
import {VaultSecret} from "../vault-secrets-operator";
import {HelmRelease} from "../../helm";
export {ArgoCdApplication} from "./app"

interface ArgoCdConfig {
    chartVersion: string;
    repo: string;
    valuesFiles?: string[];
    clientId: string;
    clientSecret: string;
}

interface GlobalConfig {
    path: string;
}

export class ArgoCd extends pulumi.ComponentResource {
    private readonly config: pulumi.Config;
    release: HelmRelease;

    constructor(name: string, opts?: pulumi.ComponentResourceOptions) {
        super("cfa-nexus:ArgoCd", name, {}, opts);
        this.config = new pulumi.Config();
        const { version, repo, valuesFiles, clientSecret, clientId } = this.getArgoCdConfig();

        const argoCd = "argocd";
        const argoCdNs = this.createArgoCdNamespace(argoCd);
        const argoCdSecret = this.createArgoCdSecret("argocd-secret", argoCd, argoCdNs);
        const hostname = this.config.require("fqdn")

        this.release = new HelmRelease(argoCd, {
            chart: "argo-cd",
            version,
            repo,
            namespace: argoCd,
            valueYamlFiles: valuesFiles,
            values: {
                server: {
                    ingress: {
                        hosts: [hostname]
                    },
                    config: {
                        url: `https://${hostname}/argo-cd`,
                        "dex.config": `connectors:
        # Auth using GitHub.
        # See https://dexidp.io/docs/connectors/github/
        - type: github
          id: github
          name: GitHub
          config:
            clientID: ${clientId}
            # Reference to key in argo-secret Kubernetes resource
            clientSecret: ${clientSecret}
            orgs:
              - name: cfa-nexus`
                    }
                }
            }
        }, {parent: this, dependsOn: [argoCdNs, argoCdSecret]});

        this.registerOutputs();
    }

    private getArgoCdConfig() {
        const argoCdConfig = this.config.requireObject<ArgoCdConfig>("argoCd");
        return {
            version: argoCdConfig.chartVersion,
            repo: argoCdConfig.repo,
            valuesFiles: (argoCdConfig.valuesFiles || ["values.yaml"])
                .map((file: string) => new pulumi.asset.FileAsset(path.join(__dirname, file))),
            clientId: argoCdConfig.clientId,
            clientSecret: argoCdConfig.clientSecret
        };
    }

    private getSecretsConfig() {
        return this.config.requireObject<GlobalConfig>("vaultSecrets");
    }

    private createArgoCdNamespace(name: string) {
        return new k8s.core.v1.Namespace(`${name}-ns`, {
            metadata: { name },
        }, { parent: this });
    }

    private createArgoCdSecret(name: string, namespace: string, dependsOn: pulumi.Input<any>) {
        const path = this.getSecretsConfig().path
        return new VaultSecret("argocd-secret", {
            spec: {
                path: `${path}/argocd`,
                type: "Opaque",
            },
            metadata: {
                namespace: namespace,
                name: name,
            },
        }, { parent: this, dependsOn });
    }
}
