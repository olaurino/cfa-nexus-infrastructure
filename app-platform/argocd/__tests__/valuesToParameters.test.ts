import * as pulumi from "@pulumi/pulumi"
import {valuesToParameters} from "../app";


describe('valuesToParameters', () => {
    const input = {
        global: {
            vaultSecretsPath: 'secret/k8s_operator/cfa-nexus.cloud',
        },
        jupyterhub: {
            hub: {
                config: {
                    Authenticator: {
                        enable_auth_state: false,
                    },
                },
            },
            debug: {
                enabled: true,
            },
            ingress: {
                hosts: ['lupalberto.sytes.net'],
                annotations: [{
                    'nginx.ingress.kubernetes.io/auth-signin': 'https://lupalberto.sytes.net/login',
                }],
            },
        },
        config: {
            base_url: 'https://lupalberto.sytes.net',
            butler_secret_path: 'secret/k8s_operator/cfa-nexus.cloud/butler-secret',
            pull_secret_path: 'secret/k8s_operator/cfa-nexus.cloud/pull-secret',
            lab_environment: {
                AUTO_REPO_URLS: 'https://github.com/lsst-sqre/system-test,https://github.com/rubin-dp0/tutorial-notebooks',
                AUTO_REPO_BRANCH: 'prod',
                AUTO_REPO_SPECS: 'https://github.com/lsst-sqre/system-test@prod,https://github.com/rubin-dp0/tutorial-notebooks@prod',
            },
            volume_mounts: [
                {name: 'home', mountPath: '/home'},
            ],
            volumes: [
                {
                    name: 'home',
                    nfs: {
                        server: '10.27.197.146',
                        path: 'homes',
                    },
                },
            ],
        },
    };

    test('should convert object values to strings', () => {
        const input = {
            key1: "value1",
            key2: 42,
            key3: true,
            key4: {
                nestedKey1: "nestedValue1",
                nestedKey2: false,
                nestedKey3: 3,
            },
        };

        const expectedResult = [
            { name: "key1", value: "value1" },
            { name: "key2", value: "42" },
            { name: "key3", value: "true" },
            { name: "key4.nestedKey1", value: "nestedValue1" },
            { name: "key4.nestedKey2", value: "false" },
            { name: "key4.nestedKey3", value: "3" },
        ];

        const output = valuesToParameters(input);

        expect(output).toEqual(expectedResult);
    });

    it('converts global.vaultSecretsPath', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({
            name: 'global.vaultSecretsPath',
            value: 'secret/k8s_operator/cfa-nexus.cloud'
        });
    });

    it('converts jupyterhub.hub.config.Authenticator.enable_auth_state', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({name: 'jupyterhub.hub.config.Authenticator.enable_auth_state', value: 'false'});
    });

    it('converts jupyterhub.debug.enabled', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({name: 'jupyterhub.debug.enabled', value: 'true'});
    });

    it('converts jupyterhub.ingress.hosts', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({name: 'jupyterhub.ingress.hosts[0]', value: "lupalberto.sytes.net"});
    });

    it('converts config.base_url', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({name: 'config.base_url', value: 'https://lupalberto.sytes.net'});
    });

    it('converts config.butler_secret_path', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({ name: 'config.butler_secret_path', value: 'secret/k8s_operator/cfa-nexus.cloud/butler-secret' });
    });

    it('converts config.pull_secret_path', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({ name: 'config.pull_secret_path', value: 'secret/k8s_operator/cfa-nexus.cloud/pull-secret' });
    });

    it('converts config.lab_environment.AUTO_REPO_URLS', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({ name: 'config.lab_environment.AUTO_REPO_URLS', value: 'https://github.com/lsst-sqre/system-test,https://github.com/rubin-dp0/tutorial-notebooks' });
    });

    it('converts config.lab_environment.AUTO_REPO_BRANCH', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({ name: 'config.lab_environment.AUTO_REPO_BRANCH', value: 'prod' });
    });

    it('converts config.lab_environment.AUTO_REPO_SPECS', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({ name: 'config.lab_environment.AUTO_REPO_SPECS', value: 'https://github.com/lsst-sqre/system-test@prod,https://github.com/rubin-dp0/tutorial-notebooks@prod' });
    });

    it('converts config.volumes', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({ name: 'config.volumes[0].name', value: "home" })
    });
});

describe('annotations', () => {
    const input = {
        jupyterhub: {
            ingress: {
                annotations: [
                    {'nginx.ingress.kubernetes.io/auth-signin': 'https://lupalberto.sytes.net/login'},
                    {'nginx.ingress.kubernetes.io/auth-signout': 'https://lupalberto.sytes.net/logout'},
                ],
            },
        },
    };

    it('converts jupyterhub.ingress.annotations', () => {
        const result = valuesToParameters(input);
        expect(result).toContainEqual({
            name: 'jupyterhub.ingress.annotations.nginx\\.ingress\\.kubernetes\\.io/auth-signin',
            value: 'https://lupalberto.sytes.net/login',
        });
        expect(result).toContainEqual({
            name: 'jupyterhub.ingress.annotations.nginx\\.ingress\\.kubernetes\\.io/auth-signout',
            value: 'https://lupalberto.sytes.net/logout',
        });
    });
});
