// import * as pulumi from "@pulumi/pulumi";
// import {ArgoCdApplication} from "../app";
// import {createMoneyPennyComponent} from "../../../applications/moneypenny";
// import {Cluster, ClusterFactory} from "../../../cluster";
// import {ProviderType} from "../../../cluster/factory";
// import * as gcp from "@pulumi/gcp";
//
// function promiseOf<T>(output: pulumi.Output<T>): Promise<T> {
//     return new Promise(resolve => output.apply(resolve));
// }
//
// pulumi.runtime.setMocks({
//         newResource: function (args: pulumi.runtime.MockResourceArgs): { id: string, state: any } {
//             return {
//                 id: args.inputs.name + "_id",
//                 state: args.inputs,
//             };
//         },
//         call: function (args: pulumi.runtime.MockCallArgs) {
//             return args.inputs;
//         }
//     },
//     "project",
//     "stack",
//     true,
// );
//
// describe("ArgoCdApplication", () => {
//
//     pulumi.runtime.setConfig("project:fqdn", "hostname")
//     pulumi.runtime.setConfig("project:vaultSecrets", `{
//     "address": "https://vault",
//     "chartVersion": "1.0.0",
//     "path": "/some/path",
//     "repo": "/https://repo","token": "4321"
//     }`)
//     pulumi.runtime.setConfig("project:phalanx", `{
//     "path": "/some/path",
//     "repo": "https://repo"
//     }`)
//     pulumi.runtime.setConfig("project:cluster", "gcp")
//     pulumi.runtime.setConfig("project:moneypenny", "{}")
//
//
//     it("creates an Argo CD Application with correct default project, sync policy and values", async () => {
//         expect.assertions(1)
//
//         const config = new pulumi.Config()
//
//         const options = {
//             name: config.require<ProviderType>("cluster"),
//             providerSpecificOptions: {}
//         }
//
//         const cluster: Cluster = ClusterFactory.createCluster(options.name, options)
//
//         const fs = cluster.getFileSystem("nfs", {
//             name: "nfs"
//         })
//
//         nfs.serverAddress = pulumi.output("1.2.3.4")
//
//         const component = createMoneyPennyComponent({nfs})
//
//         const inputs = (await promiseOf(component.argoCdApp!.app)).getInputs()
//
//         const expectedHelmParameters = [
//             {name: "global.host", value: "hostname"},
//             {name: "global.baseUrl", value: "https://hostname"},
//             {name: "global.vaultSecretsPath", value: "/some/path"},
//             {name: "volumes[0].name", value: "homedirs"},
//             {name: "volumes[0].nfs.server", value: "1.2.3.4"},
//             {name: "volumes[0].nfs.path", value: "/homes"},
//         ]
//
//         expect(inputs).toMatchObject({
//             apiVersion: "argoproj.io/v1alpha1",
//             kind: "Application",
//             metadata: {
//                 namespace: "argocd",
//                 name: "moneypenny",
//             },
//             spec: {
//                 destination: {
//                     namespace: "moneypenny",
//                     server: "https://kubernetes.default.svc",
//                 },
//                 project: "default",
//                 source: {
//                     helm: {
//                         parameters: expectedHelmParameters,
//                         valueFiles: ["values.yaml", "../../../applications/moneypenny/values.yaml"],
//                     },
//                     path: "/some/path/moneypenny",
//                     repoURL: "https://repo",
//                     targetRevision: "master",
//                 },
//                 syncPolicy: {
//                     automated: {
//                         prune: true,
//                         selfHeal: true,
//                     },
//                     syncOptions: ["CreateNamespace=true", "Validate=true"],
//                 },
//             },
//         });
//     })
// })
// ;
