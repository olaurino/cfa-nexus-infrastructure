import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

function getOutputs(obj: any): pulumi.Output<any>[] {
    const result = []

    if (typeof obj === "object") {
        for (const key in obj) {
            if (pulumi.Output.isInstance(obj[key])) {
                result.push(obj[key])
            } else if (typeof obj[key] === "object") {
                result.push(...getOutputs(obj[key]));
            }
        }
    }

    return result;
}

export interface ArgoCdAppArgs {
    name: string;
    path: string;
    repoUrl: string;
    namespace: string;
    project?: string;
    values?: pulumi.Input<any>;
    valueYamlFiles?: string[];
    version: string;
    volumes?: any;
    wave: string;
}

export class ArgoCdApplication extends pulumi.ComponentResource {
    public readonly app: pulumi.Output<k8s.apiextensions.CustomResource>;

    constructor(
        name: string,
        args: ArgoCdAppArgs,
        opts?: pulumi.ComponentResourceOptions
    ) {
        super("cfa-nexus:ArgoCdApplication", name, {}, opts);

        const appManifest = {
            apiVersion: "argoproj.io/v1alpha1",
            kind: "Application",
            metadata: {
                namespace: "argocd",
                name: name,
                annotations: {
                    "argocd.argoproj.io/sync-wave": args.wave
                }
            },
            spec: {
                destination: {
                    namespace: args.namespace,
                    server: "https://kubernetes.default.svc",
                },
                project: args.project || "default",
                source: {
                    path: args.path,
                    repoURL: args.repoUrl,
                    targetRevision: args.version
                },
                volumes: args.volumes,
                syncPolicy: {
                    automated: {
                        prune: true,
                        selfHeal: true,
                    },
                    syncOptions: ["CreateNamespace=true", "Validate=true"],
                },
            },
        }

        if (args.valueYamlFiles) {
            // @ts-ignore
            appManifest.spec.source.helm = {
                parameters: valuesToParameters(args.values),
                valueFiles: args.valueYamlFiles,
            };
        } else {
            // @ts-ignore
            appManifest.spec.source.directory = {
                recurse: true,
            };
        }

        this.app = pulumi.all(getOutputs(appManifest)).apply(() => {
            return pulumi.output(appManifest).apply(man => {
                return new k8s.apiextensions.CustomResource(name, man, {parent: this});
            })
        })

        this.registerOutputs();
    }
}

function processAnnotations(values: any[], path: string[]): any[] {
    let result: any[] = [];

    for (const annotationObj of values) {
        for (const key in annotationObj) {
            const paramName = path.concat(key.replaceAll('.', '\\.')).join('.');
            result.push({name: paramName, value: annotationObj[key]});
        }
    }

    return result;
}

function processObjects(values: any, key: string, path: string[]): any[] {
    if (pulumi.Output.isInstance(values[key])) {
        return processScalars(values, key, path, false)
    }
    return valuesToParameters(values[key], path.concat(key));
}

function processArrays(values: any, key: string, path: string[]): any[] {
    return values[key].map((item: any, index: number) => {
        if (typeof item === "object" && !Array.isArray(item)) {
            return valuesToParameters(item, path.concat(`${key}[${index}]`));
        } else {
            const paramName = path.concat(`${key}[${index}]`).join(".");
            return {name: paramName, value: item};
        }
    }).flat();
}

function processScalars(values: any, key: string, path: string[], string: boolean = true): any[] {
    const paramName = path.concat(key).join(".");
    const paramValue = string ? String(values[key]) : values[key];
    return [{name: paramName, value: paramValue}];
}

export function valuesToParameters(values: any, path: string[] = []): any[] {
    let result: any[] = [];

    for (const key in values) {
        if (key === "annotations") {
            result = result.concat(processAnnotations(values[key], path.concat(key)));
        } else if (typeof values[key] === "object" && !Array.isArray(values[key])) {
            result = result.concat(processObjects(values, key, path));
        } else if (Array.isArray(values[key])) {
            result = result.concat(processArrays(values, key, path));
        } else {
            result = result.concat(processScalars(values, key, path));
        }
    }

    return result;
}