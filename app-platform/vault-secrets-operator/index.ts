import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import {Namespace} from "@pulumi/kubernetes/core/v1";
import {CustomResource, CustomResourceOptions} from "@pulumi/pulumi";
import {HelmRelease} from "../../helm";

export interface VaultSecretsConfig {
    chartVersion: string;
    repo: string;
    token: pulumi.Input<string>;
    address: pulumi.Input<string>;
}

export interface VaultSecretsOperatorArgs {
    // cluster: Cluster;
}

export class VaultSecretsOperator extends pulumi.ComponentResource {
    public readonly release: k8s.helm.v3.Release;
    private readonly config: VaultSecretsConfig;

    constructor(name: string, args: VaultSecretsOperatorArgs, opts?: pulumi.ComponentResourceOptions) {
        super("cfa-nexus:VaultSecretsOperator", name, {}, opts);

        this.config = new pulumi.Config().requireObject<VaultSecretsConfig>("vaultSecrets");
        const vaultOperatorNs = this.makeNamespace();
        const vaultSecret = this.makeSecret(vaultOperatorNs);
        const helmReleaseArgs = {
            valueYamlFiles: [],
            chart: "vault-secrets-operator",
            version: this.config.chartVersion,
            repo: this.config.repo,
            namespace: "vault-secrets-operator",
            // provider: args.cluster.provider,
            values: {
                "environmentVars": [
                    {
                        "name": "VAULT_TOKEN",
                        "valueFrom": {
                            "secretKeyRef": {
                                "name": "vault-secrets-operator",
                                "key": "VAULT_TOKEN",
                            },
                        },
                    },
                    {
                        "name": "VAULT_TOKEN_LEASE_DURATION",
                        "valueFrom": {
                            "secretKeyRef": {
                                "name": "vault-secrets-operator",
                                "key": "VAULT_TOKEN_LEASE_DURATION",
                            },
                        },
                    },
                ],
                "vault": {
                    "address": this.config.address,
                    "reconciliationTime": 60,
                },
            },
            dependencies: [vaultSecret]
        };
        this.release = new HelmRelease(name, helmReleaseArgs, { parent: this }).release;
    }

    private makeSecret(vaultOperatorNs: Namespace) {
        return new k8s.core.v1.Secret("vault-secrets-operator-secret", {
            metadata: {
                name: "vault-secrets-operator",
                namespace: "vault-secrets-operator",
            },
            data: {
                "VAULT_TOKEN": pulumi.secret(this.config.token).apply(v => Buffer.from(v).toString("base64")),
                "VAULT_TOKEN_LEASE_DURATION": pulumi.secret("31536000").apply(s => Buffer.from(s).toString("base64")),
            },
            type: "Opaque",
        }, {dependsOn: vaultOperatorNs, parent: this});
    }

    private makeNamespace() {
        return new k8s.core.v1.Namespace("vault-secrets-operator-ns", {
            metadata: {name: "vault-secrets-operator"},
        }, {parent: this});
    }
}

interface VaultSecretSpec {
    path: string;
    type: string;
}

interface VaultSecretArgs {
    metadata?: k8s.types.input.meta.v1.ObjectMeta;
    spec: VaultSecretSpec;
}

export class VaultSecret extends CustomResource {
    constructor(name: string, args: VaultSecretArgs, opts?: CustomResourceOptions) {
        const {metadata, spec} = args;

        const resourceArgs = {
            apiVersion: "ricoberger.de/v1alpha1",
            kind: "VaultSecret",
            metadata,
            spec
        };

        super("kubernetes:ricoberger.de/v1alpha1:VaultSecret", name, resourceArgs, opts);
    }
}