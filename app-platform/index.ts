import {VaultSecretsOperator} from "./vault-secrets-operator";
import {IngressNginx} from "./ingress-nginx";
import {ArgoCd} from "./argocd"
import * as pulumi from "@pulumi/pulumi";
import {Cluster} from "../cluster";

export {
    VaultSecretsOperator,
    IngressNginx,
    ArgoCd
}

export class ApplicationPlatform extends pulumi.ComponentResource {
    readonly vaultSecretsOperator: VaultSecretsOperator;
    readonly ingressNginx: IngressNginx;
    readonly argoCd: ArgoCd;

    constructor(name: string, cluster: Cluster, opts?: pulumi.ComponentResourceOptions) {
        super("cfa-nexus:ApplicationPlatform", name, {}, opts);

        this.vaultSecretsOperator = new VaultSecretsOperator("vault-secrets-operator", {},
            {dependsOn: cluster, parent: this});

        this.ingressNginx = new IngressNginx("ingress-nginx", cluster, {
            dependsOn: [this.vaultSecretsOperator, cluster],
            parent: this
        });

        this.argoCd = new ArgoCd("argocd", {
            dependsOn: [this.ingressNginx, this.vaultSecretsOperator],
            parent: this
        });

        this.registerOutputs();
    }
}