import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";
import { Cluster } from "../../cluster";
import * as path from "path";
import { FileAsset } from "@pulumi/pulumi/asset";
import { VaultSecret } from "../vault-secrets-operator";
import {HelmRelease} from "../../helm";

interface IngressNginxConfig {
    repo: string;
    chartVersion: string;
}

interface VaultSecretsConfig {
    path: string;
}

export class IngressNginx extends pulumi.ComponentResource {
    private readonly config: IngressNginxConfig;
    private readonly secretsPath: string;
    release: HelmRelease;
    private cluster: Cluster;

    constructor(name: string, cluster: Cluster, opts?: pulumi.ComponentResourceOptions) {
        super("cfa-nexus:IngressNginx", name, {}, opts);

        this.cluster = cluster
        const config = new pulumi.Config();
        this.config = config.requireObject<IngressNginxConfig>("ingressNginx");
        this.secretsPath = config.requireObject<VaultSecretsConfig>("vaultSecrets").path;

        const ingressName = "ingress-nginx";
        const ingressNs = new k8s.core.v1.Namespace(`${ingressName}-ns`, {
            metadata: { name: ingressName },
        }, { parent: this });

        const ingressCertificate = new VaultSecret("ingress-certificate", {
            spec: {
                path: `${this.secretsPath}/ingress-nginx`,
                type: "kubernetes.io/tls",
            },
            metadata: {
                namespace: ingressName,
                name: ingressName,
            },
        }, { parent: this, dependsOn: ingressNs });

        const chartArgs = {
            chart: ingressName,
            version: this.config.chartVersion,
            repo: this.config.repo,
            namespace: ingressName,
            valueYamlFiles: [new FileAsset(path.join(__dirname, "values.yaml"))],
            values: {
                controller: {
                    service: {
                        loadBalancerIP: this.cluster.reservedIP.value
                    },
                    extraArgs: {
                        "default-ssl-certificate": "ingress-nginx/ingress-nginx"
                    },
                    config: {
                        "compute-full-forwarded-for": "true",
                        "large-client-header-buffers": "4 64k",
                        "proxy-body-size": "100m",
                        "proxy-buffer-size": "64k",
                        "ssl-redirect": "true",
                        "use-forwarded-headers": "true",
                        "externalTrafficPolicy": "Local",
                    },
                    podLabels: {
                        "gafaelfawr.lsst.io/ingress": "true",
                        "hub.jupyter.org/network-access-proxy-http": "true"
                    },
                    metrics: {
                        enabled: true
                    }
                }
            }
        };

        this.release = new HelmRelease(ingressName, chartArgs, { dependsOn: [ingressCertificate], parent: this });

        this.registerOutputs();
    }
}
