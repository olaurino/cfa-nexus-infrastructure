import * as pulumi from "@pulumi/pulumi";
import {Cluster, ResourceWrapper, Storage} from "./types";
import {GKECluster} from "./gcp";
import * as k8s from "@pulumi/kubernetes";
import {HostPathFs} from "./local/hostPath";

interface ClusterOptions<T> {
    name: ProviderType;
    providerSpecificOptions: T;
}

interface LocalOptions {}
interface GcpOptions {
    engineVersion: string
}

export type ProviderType = "local" | "gcp"

type ProviderTypeToOptionsMap = {
    local: LocalOptions;
    gcp: GcpOptions;
};

export class ClusterFactory {
    public static createCluster<T extends keyof ProviderTypeToOptionsMap>(
        providerType: ProviderType, options: ClusterOptions<ProviderTypeToOptionsMap[T]>
    ): Cluster {
        switch (providerType) {
            case "local":
                return this.createLocalCluster(options);
            case "gcp":
                // @ts-ignore
                return this.createGcpCluster(options)
            default:
                throw new Error(`Unsupported provider type: ${providerType}`);
        }
    }

    private static createLocalCluster(
        _: ClusterOptions<LocalOptions>
    ): Cluster {

        return new LocalCluster(
            {value: pulumi.output("172.26.171.32")},
            {value: pulumi.output("default")},
        );
    }

    private static createGcpCluster(
        options: ClusterOptions<GcpOptions>
    ): Cluster {

        return new GKECluster("gke-cluster", {
            clusterName: "pulumi-cluster",
            engineVersion: options.providerSpecificOptions.engineVersion,
        });
    }
}

class LocalCluster extends pulumi.Resource implements Cluster {
    fileSystems: Map<string, Storage> = new Map();

    constructor(public reservedIP: ResourceWrapper<string>, public network: ResourceWrapper<string>){
        super("cfa-nexus:LocalCluster", "local", false)
    }

    provider: k8s.Provider = new k8s.Provider("local");

    getFileSystem(name: string): Storage {
        if (!this.fileSystems.has(name)) {
            const fs = new HostPathFs(name, {}, {parent: this});
            this.fileSystems.set(name, fs);
        }

        return this.fileSystems.get(name)!;
    }


}