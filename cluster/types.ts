import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface ResourceWrapper<T> {
    value: pulumi.Output<T>,
    resource?: pulumi.Resource
}

export interface Cluster extends pulumi.Resource {
    reservedIP: ResourceWrapper<string>;
    network: ResourceWrapper<string>;

    provider: k8s.Provider

    getFileSystem(name: string): Storage
}

export interface NFS extends pulumi.ComponentResource {

    resource: pulumi.Resource

    getStorage(name: string): unknown
}

export interface HostPath extends pulumi.ComponentResource {
    getStorage(name: string): unknown

    resource: pulumi.Resource
}

export type Storage = NFS | HostPath;