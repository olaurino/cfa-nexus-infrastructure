import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes"
import * as gcp from "@pulumi/gcp";
import {Cluster, Storage, ResourceWrapper} from "../types";
import {GoogleNFS} from "./nfs";

export interface GKEClusterArgs {
    clusterName: string;
    engineVersion: string;
}

export class GKECluster extends pulumi.ComponentResource implements Cluster {
    public readonly network: ResourceWrapper<string>;
    public readonly subnetwork: gcp.compute.Subnetwork;
    public readonly k8sCluster: gcp.container.Cluster;
    public readonly nodePools: gcp.container.NodePool[] = [];
    public readonly reservedIP: ResourceWrapper<string>;

    public readonly fileSystems: Map<string, Storage> = new Map();

    public readonly provider: k8s.Provider

    constructor(name: string, args: GKEClusterArgs, opts?: pulumi.ComponentResourceOptions) {
        super("cfa-nexus:GKECluster", name, {}, opts);

        const config = new pulumi.Config("gcp");
        const region = config.get("region") || "us-central1";

        const network = new gcp.compute.Network(`${name}-network`, {}, {parent: this});
        this.network = {value: network.name, resource: network}

        this.subnetwork = new gcp.compute.Subnetwork(`${name}-subnetwork`, {
            network: network.id,
            ipCidrRange: "10.2.0.0/16",
            region: region,
        }, {dependsOn: network, parent: this});

        this.k8sCluster = new gcp.container.Cluster(name, {
            name: args.clusterName,
            initialNodeCount: 1,
            clusterAutoscaling: {
                autoscalingProfile: "OPTIMIZE_UTILIZATION",
                enabled: true,
                resourceLimits: [
                    {resourceType: "cpu", minimum: 0.1, maximum: 64},
                    {resourceType: "memory", minimum: 512, maximum: 65536}
                ],
            },
            removeDefaultNodePool: true,
            minMasterVersion: args.engineVersion,
            network: network.id,
            subnetwork: this.subnetwork.id,
            location: region,
        }, {dependsOn: this.subnetwork, parent: this});

        this.nodePools.push(new gcp.container.NodePool(name, {
            cluster: this.k8sCluster.name,
            initialNodeCount: 1,
            location: region,
            autoscaling: {
                minNodeCount: 1,
                maxNodeCount: 10,
            },
            nodeConfig: {
                machineType: "e2-standard-2",
                oauthScopes: [
                    "https://www.googleapis.com/auth/compute",
                    "https://www.googleapis.com/auth/devstorage.read_only",
                    "https://www.googleapis.com/auth/logging.write",
                    "https://www.googleapis.com/auth/monitoring",
                ],
            },
            version: args.engineVersion,
        }, {dependsOn: this.k8sCluster, parent: this}));

        this.nodePools.push(new gcp.container.NodePool(`${name}-jupyter`, {
            cluster: this.k8sCluster.name,
            initialNodeCount: 0,
            location: region,
            nodeLocations: ["us-central1-a"],
            autoscaling: {
                minNodeCount: 0,
                maxNodeCount: 10,
            },
            nodeConfig: {
                taints: [
                    {key: "hub.jupyter.org/dedicated", value: "user", effect: "NO_SCHEDULE"}
                ],
                labels: {
                    nodeType: "jupyter",
                },
                machineType: "n2-standard-8",
                oauthScopes: [
                    "https://www.googleapis.com/auth/compute",
                    "https://www.googleapis.com/auth/devstorage.read_only",
                    "https://www.googleapis.com/auth/logging.write",
                    "https://www.googleapis.com/auth/monitoring",
                ],

            },
            version: args.engineVersion,
        }, {dependsOn: this.k8sCluster, parent: this}));

        const kubeconfig = pulumi.all([this.k8sCluster.name, this.k8sCluster.endpoint, this.k8sCluster.masterAuth]).apply(([name, endpoint, masterAuth]) => {
            const context = `${gcp.config.project}_${gcp.config.zone}_${name}`;
            return `apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${masterAuth.clusterCaCertificate}
    server: https://${endpoint}
  name: ${context}
contexts:
- context:
    cluster: ${context}
    user: ${context}
  name: ${context}
current-context: ${context}
kind: Config
preferences: {}
users:
- name: ${context}
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      command: gke-gcloud-auth-plugin
      installHint: Install gke-gcloud-auth-plugin for use with kubectl by following
        https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke
      provideClusterInfo: true
`;
        });

        this.provider = new k8s.Provider(name, {
            kubeconfig: kubeconfig,
        });

        const reservedIP = new gcp.compute.Address(`${name}-ingress-nginx-lb-ip`, {
            addressType: "EXTERNAL",
        }, {dependsOn: this.k8sCluster, parent: this});

        this.reservedIP = {value: reservedIP.address, resource: reservedIP}

        this.registerOutputs();
    }

    getFileSystem(name: string): Storage {
        if (!this.fileSystems.has(name)) {
            const args = {
                name: name,
                region: "us-central1-b",
                network: this.network.value,
            }
            const fs = new GoogleNFS(name, args, {parent: this});
            this.fileSystems.set(name, fs);
        }

        return this.fileSystems.get(name)!
    }

}