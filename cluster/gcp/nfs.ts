import * as pulumi from "@pulumi/pulumi";
import * as gcp from "@pulumi/gcp";
import {NFS} from "../types";

export interface NFSProviderArgs {
    name: string;
    region?: string;
    network: pulumi.Input<string>;
}

export class GoogleNFS extends pulumi.ComponentResource implements NFS {
    readonly storage

    readonly resource: this

    constructor(name: string, args: NFSProviderArgs, opts?: pulumi.ComponentResourceOptions) {
        super("cfa-nexus:GoogleNFS", name, {}, opts)
        this.resource = this
        const instance = new gcp.filestore.Instance(args.name, {
            fileShares: {
                capacityGb: 1024,
                name: args.name,
            },
            location: args.region,
            networks: [{
                modes: ["MODE_IPV4"],
                network: args.network,
            }],
            tier: "BASIC_HDD",
        }, {parent: this});

        this.storage = {
            server: instance.networks.apply((networks) => networks[0].ipAddresses[0]),
            type: "nfs",
            serverPath: `/${args.name}`
        }

        this.registerOutputs({
            storage: this.storage
        })
    }

    getStorage(name: string): unknown {
        return {
            name,
            source: this.storage
        };
    }
}
