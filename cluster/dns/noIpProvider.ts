import * as pulumi from "@pulumi/pulumi";
import axios from "axios";
import {AxiosResponse} from "axios";

export type SimpleResourceProviderArgs = {
    hostname: string;
    ip: string;
    username: string;
    password: string;
};

export type SimpleResourceArgs = {
    hostname: pulumi.Input<string>;
    ip: pulumi.Input<string>;
    username: pulumi.Input<string>;
    password: pulumi.Input<string>;
};

type SimpleResourceOutputs = {
    ip: string,
    hostname: string
}

class SimpleResourceProvider implements pulumi.dynamic.ResourceProvider {
    async create(inputs: SimpleResourceProviderArgs): Promise<pulumi.dynamic.CreateResult> {
        await pulumi.log.debug(`provider ip ${inputs.ip} host: ${inputs.hostname}`)

        const response = await axios.get("https://dynupdate.no-ip.com/nic/update", {
            params: {
                hostname: inputs.hostname,
                myip: inputs.ip,
            },
            auth: {
                username: inputs.username,
                password: inputs.password,
            },
        });

        if (!await this.isResponseOk(response)) {
            throw new Error(`Failed to update No-IP DNS: ${response.statusText} - ${response.data}`);
        }
        await pulumi.log.debug(`response: ${response.data}`)
        await pulumi.log.debug(`created ip ${inputs.ip} host ${inputs.hostname}`)
        return {id: `${inputs.hostname}-${inputs.ip}`, outs: {ip: inputs.ip, hostname: inputs.hostname}};
    }

    private async isResponseOk(response: AxiosResponse<any>): Promise<boolean> {
        const { data, status } = response

        if (status !== 200) {
            await pulumi.log.error(`Status is: ${status}`)
            return false
        }

        if (data.includes("nochg")) {
            await pulumi.log.warn(`Unchanged: ${data}`)
            return true
        }

        if (!(data.includes("OK") || data.includes("good"))) {
            await pulumi.log.error(`Response is not OK: ${data}`)
            return false
        }

        return true
    }

    async update(id: pulumi.ID, olds: SimpleResourceProviderArgs, news: SimpleResourceProviderArgs): Promise<pulumi.dynamic.UpdateResult> {
        await pulumi.log.debug(`old ip ${olds.ip} host ${olds.hostname}`)
        await pulumi.log.debug(`new ip ${news.ip} host ${news.hostname}`)
        return this.create(news);
    }

    async diff(id: pulumi.ID, olds: SimpleResourceOutputs, news: SimpleResourceOutputs): Promise<pulumi.dynamic.DiffResult> {
        const replaces: string[] = [];

        await pulumi.log.debug(`old ip ${olds.ip} host ${olds.hostname}`)
        await pulumi.log.debug(`new ip ${news.ip} host ${news.hostname}`)

        if (olds.hostname !== news.hostname) {
            replaces.push("hostname");
        }
        if (olds.ip !== news.ip) {
            replaces.push("ip");
        }

        return {
            changes: replaces.length > 0,
            replaces: replaces,
        };
    }

}

export class SimpleResource extends pulumi.dynamic.Resource {
    constructor(name: string, args: SimpleResourceArgs, opts?: pulumi.CustomResourceOptions) {
        pulumi.output(args).apply(i => pulumi.log.debug(`resource ${i.ip} host: ${i.hostname}`))
        super(new SimpleResourceProvider(), name, args, opts);
    }
}