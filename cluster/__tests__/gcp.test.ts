import * as pulumi from "@pulumi/pulumi";
import {GKECluster} from "../gcp";
import {NFS} from "../types";

describe("GKECluster", () => {
    let cluster: GKECluster;

    beforeAll(async () => {
        pulumi.runtime.setMocks({
                newResource: function(args: pulumi.runtime.MockResourceArgs): {id: string, state: any} {
                    if (args.type == "gcp:compute/address:Address") {
                        console.error(JSON.stringify(args.inputs));
                        args.inputs.address = pulumi.output("172.0.0.");
                    }
                    if (args.type === "gcp:filestore/instance:Instance") {
                        args.inputs.networks[0].ipAddresses = ["10.0.0.1"];
                    }
                    if (args.type === "gcp:container/cluster:Cluster") {
                        args.inputs.endpoint = "foo";
                        args.inputs.masterAuth = {clusterCaCertificate: "someCert"};
                    }
                    return {
                        id: args.inputs.name + "_id",
                        state: args.inputs,
                    };
                },
                call: function(args: pulumi.runtime.MockCallArgs) {
                    return args.inputs;
                },
            },
            "project",
            "stack",
            false, // Sets the flag `dryRun`, which indicates if pulumi is running in preview mode.
        );
        cluster = new GKECluster("test-cluster", {
            clusterName: "test-gke-cluster",
            engineVersion: "1.21",
        });
    });
    //
    // test("should have the correct name", () => {
    //     expect(cluster.k8sCluster.name).toEqual("test-gke-cluster");
    // });
    //
    // test("should have the correct engine version", () => {
    //     expect(cluster.k8sCluster.nodeVersion).toEqual("1.21");
    //     expect(cluster.k8sCluster.minMasterVersion).toEqual("1.21");
    // });
    //
    // test("should create a network and subnetwork", async () => {
    //     expect(cluster.network.value).toEqual("test-cluster-network");
    //     expect(cluster.subnetwork.network).toEqual("test-cluster-network_id");
    // });

    test("should create a reserved IP", async () => {
        expect.assertions(1);

        cluster.reservedIP.value.apply(v => {
            expect(v).toEqual("test-cluster-ingress-nginx-lb-ip");
        })
    });
    //
    // test("should create a file system", async () => {
    //     const fileSystem = cluster.getFileSystem("test-file-system");
    //     expect(fileSystem.volume.name).toEqual("test-file-system");
    //     expect((<NFS>fileSystem).volume.nfs.server).toEqual("10.0.0.2");
    //     expect((<NFS>fileSystem).volume.nfs.path).toEqual("/home");
    // });
});
