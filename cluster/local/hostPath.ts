import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes";
import {HostPath} from "../types";

export class HostPathFs extends pulumi.ComponentResource implements HostPath {
    readonly volume

    readonly persistentVolume

    readonly resource: this

    constructor(name: string, {}, opts?: pulumi.ComponentResourceOptions) {
        super("cfa-nexus:HostPathStorage", name, {}, opts)
        this.resource = this;

        this.persistentVolume = new k8s.core.v1.PersistentVolume(name, {
            spec: {
                capacity: {
                    storage: "1Gi",
                },
                accessModes: ["ReadWriteOnce"],
                persistentVolumeReclaimPolicy: "Retain",
                hostPath: {
                    path: `/mnt/${name}`,
                },
            },
        });

        this.volume = {
            type: 'hostPath',
            path: `/${name}`
        }
    }

    getStorage(name: string): unknown {
        return {
            name,
            source: this.volume
        }
    }
}