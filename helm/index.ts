import * as pulumi from "@pulumi/pulumi";
import {FileAsset} from "@pulumi/pulumi/asset";
import * as k8s from "@pulumi/kubernetes";

interface CommonChartArgs {
    namespace: string;
    values?: pulumi.Input<any>;
    valueYamlFiles?: FileAsset[];
}

interface LocalChartArgs extends CommonChartArgs {
    chartPath: string;
}

interface RemoteChartArgs extends CommonChartArgs {
    chart: string;
    version: string;
    repo: string;
}

type ChartArgs = LocalChartArgs | RemoteChartArgs;

function isLocalChartArgs(args: ChartArgs): args is LocalChartArgs {
    return 'chartPath' in args;
}

function isRemoteChartArgs(args: ChartArgs): args is RemoteChartArgs {
    return 'repo' in args && 'chart' in args;
}

export class HelmRelease extends pulumi.ComponentResource {
    public readonly release: k8s.helm.v3.Release;

    constructor(name: string, args: ChartArgs, opts?: pulumi.ComponentResourceOptions) {
        super("cfa-nexus:HelmRelease", name, {}, opts);

        let releaseArgs: pulumi.Input<k8s.helm.v3.ReleaseArgs>;

        if (isLocalChartArgs(args)) {
            releaseArgs = {
                name,
                chart: args.chartPath,
                namespace: args.namespace,
                createNamespace: true,
                dependencyUpdate: true,
                values: args.values,
                valueYamlFiles: args.valueYamlFiles,
            };
        } else if (isRemoteChartArgs(args)) {
            releaseArgs = {
                name,
                chart: args.chart,
                version: args.version,
                repositoryOpts: args.repo ? { repo: args.repo } : undefined,
                namespace: args.namespace,
                createNamespace: true,
                dependencyUpdate: true,
                values: args.values,
                valueYamlFiles: args.valueYamlFiles,
            };
        } else {
            throw new Error("Invalid chart arguments");
        }

        this.release = new k8s.helm.v3.Release(name, releaseArgs, {
            // provider: args.provider,
            parent: this,
        });
    }
}


