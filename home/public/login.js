async function getUserInfo() {
    const response = await fetch('/auth/api/v1/user-info');
    const data = await response.json();
    return data;
}

function getLoginUrl(pageUrl) {
    return `/login?redirect=${encodeURIComponent(pageUrl)}`;
}

function getLogoutUrl(pageUrl) {
    return `/logout?redirect=${encodeURIComponent(pageUrl)}`;
}

async function renderLogin(pageUrl) {
    const userInfo = await getUserInfo();
    const isLoggedIn = userInfo && userInfo.hasOwnProperty('username');

    const loginElement = document.querySelector('#login');
    if (!isLoggedIn) {
        loginElement.innerHTML = `<a href="${getLoginUrl(pageUrl)}">Log in</a>`;
    } else {
        const userMenuHtml = `
      <button id="user-menu-button">
        ${userInfo.username}
      </button>
      <div id="user-menu" style="display: none;">
        <a href="${getLogoutUrl(pageUrl)}">Log out</a>
      </div>
    `;
        loginElement.innerHTML = userMenuHtml;

        const userMenuButton = document.querySelector('#user-menu-button');
        const userMenu = document.querySelector('#user-menu');

        userMenuButton.addEventListener('click', () => {
            userMenu.style.display = userMenu.style.display === 'none' ? 'block' : 'none';
        });
    }
}

renderLogin('/').then(() => console.log("rendered"));