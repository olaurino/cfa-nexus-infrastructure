import * as pulumi from "@pulumi/pulumi";
import {PhalanxComponent} from "../phalanx";

export function createGafaelfawrComponent(
    args: {},
    opts?: pulumi.ComponentResourceOptions,
    name?: string
): PhalanxComponent {
    name = name || "gafaelfawr"
    const config = new pulumi.Config();
    const fqdn = config.require("fqdn")
    const gafConfig = config.requireObject<{ storageClass: string, clientId: string, secretsPath: string }>(name)

    // const secretsPath = config.requireObject<{ path: string }>("vaultSecrets").path
    const values = {
        global: {
            host: fqdn,
            baseUrl: `https://${fqdn}`,
            vaultSecretsPath: gafConfig.secretsPath
        },
        redis: {
            persistence: {
                storageClass: gafConfig.storageClass
            }
        },
        config: {
            updateSchema: true,
            github: {
                clientId: gafConfig.clientId
            }
        }
    }

    return new PhalanxComponent("cfa-nexus:Gafaelfawr", name, values, opts);
}
