import * as pulumi from "@pulumi/pulumi";
import {ComponentResourceOptions} from "@pulumi/pulumi";
import {ArgoCdApplication} from "../app-platform/argocd";
import {ArgoCdAppArgs} from "../app-platform/argocd/app";

export interface PhalanxAppConfig {
    chart?: string;
    namespace?: string;
    values?: string[];
    repoUrl: string;
    revision: string;
}

const instances = new Map<string, PhalanxComponent>();

type PhalanxConfig = {
    repo: string,
    path: string,
    wave: string,
};

export class PhalanxComponent extends pulumi.ComponentResource {
    summary

    argoCdApp?: ArgoCdApplication;

    constructor(resourceType: string, name: string, values: any, opts?: pulumi.ComponentResourceOptions) {
        super(resourceType, name, {}, opts);
        this.summary = {
            name,
            resourceType,
            urn: this.urn
        }
        const resourceId = `${resourceType}:${name}`

        if (!instances.get(resourceId)) {
            this.argoCdApp = this.makeArgoApp(name, values, opts);
            this.registerOutputs();
            instances.set(resourceId, this);
        }

        return instances.get(resourceId) as PhalanxComponent;

    }

    private makeArgoApp(name: string, values: unknown, opts?: ComponentResourceOptions) {

        const appOptions = this.getArgoAppOptions(name, values);

        return new ArgoCdApplication(name, {
            namespace: appOptions.namespace,
            path: appOptions.path,
            repoUrl: appOptions.repoUrl,
            name,
            version: appOptions.version,
            valueYamlFiles: appOptions.valueYamlFiles,
            values: appOptions.values,
            wave: appOptions.wave
        }, {...opts, parent: this});
    }

    private getArgoAppOptions(name: string, values: unknown): ArgoCdAppArgs {
        const config = new pulumi.Config();
        const appConfig = config.requireObject<PhalanxAppConfig>(name);
        const valueFiles = appConfig.values || ["values.yaml", `../../../applications/${name}/values.yaml`];
        const phalanxConfig = config.requireObject<PhalanxConfig>("phalanx")

        return {
            namespace: name,
            name: name,
            version: appConfig.revision || "master",
            path: `${phalanxConfig.path}/${name}`,
            repoUrl: phalanxConfig.repo,
            valueYamlFiles: valueFiles,
            values,
            wave: phalanxConfig.wave
        };
    }
}
