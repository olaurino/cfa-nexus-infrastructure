import * as pulumi from "@pulumi/pulumi"
import {PhalanxComponent} from "../phalanx";

export function createPortalComponent(
    args: {},
    opts?: pulumi.ComponentResourceOptions,
    name?: string
): PhalanxComponent {
    name = name || "portal";
    const config = new pulumi.Config();
    const fqdn = config.require("fqdn");
    const vaultPathPrefix = config.requireObject<{ path: string }>("vaultSecrets").path;

    const values = {
        global: {
            host: fqdn,
            baseUrl: `https://${fqdn}`,
            vaultSecretsPath: vaultPathPrefix,
        },
        image: {
            repository: "olaurino/suit",
            tag: "2024-05-29.0"
        },
        nodeSelector: {
            nodeType: "jupyter"
        },
        tolerations: [
            {
                key: "hub.jupyter.org/dedicated",
                operator: "Equal",
                value: "user",
                effect: "NoSchedule",
            }
        ],
        resources: {
            limits: {
                cpu: 2,
                memory: "8Gi"
            },
            requests: {
                cpu: "2",
                memory: "4Gi"
            }
        },
        redis: {
            resources: {
                requests: {
                    cpu: "100m",
                }
            }
        }
    }

    return new PhalanxComponent("cfa-nexus:Portal", name, values, opts);
}
