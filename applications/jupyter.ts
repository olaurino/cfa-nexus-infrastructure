import * as pulumi from "@pulumi/pulumi"
import {Cluster, Storage} from "../cluster/types";
import {PhalanxComponent} from "./phalanx";
import {applicationFactory} from "./index";

export interface JupyterArgs {
    cluster: Cluster;
    ingress: pulumi.Resource;
}

export class Jupyter extends pulumi.ComponentResource {
    readonly storage: Storage;
    readonly nublado: PhalanxComponent;

    constructor(name: string, args: JupyterArgs, opts?: pulumi.ComponentResourceOptions) {
        super("cfa-nexus:Jupyter", name, {}, opts);

        this.storage = args.cluster.getFileSystem("homes");

        this.nublado = applicationFactory.createComponent(
            "nublado",
            args,
            { parent: this, dependsOn: [this.storage] }
        );

        this.registerOutputs();
    }
}
