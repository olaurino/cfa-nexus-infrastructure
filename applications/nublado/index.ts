import * as pulumi from "@pulumi/pulumi"
import {PhalanxComponent} from "../phalanx";
import {Cluster} from "../../cluster";

interface NubladoConfig {
    cluster: Cluster;
}

export function createNubladoComponent(
    args: NubladoConfig,
    opts?: pulumi.ComponentResourceOptions,
    name?: string
): PhalanxComponent {
    name = name || "nublado";
    const config = new pulumi.Config();
    const vaultPathPrefix = config.requireObject<{ path: string }>("vaultSecrets").path;
    const fqdn = config.require("fqdn");

    const values = {
        global: {
            host: fqdn,
            vaultSecretsPath: vaultPathPrefix,
            baseUrl: `https://${fqdn}`
        },
        jupyterhub: {
            scheduling: {
                userScheduler: {
                    enabled: true
                }
            },
            hub: {
                db: {
                    upgrade: true,
                    url: "postgresql://nublado3@postgres.postgres/nublado3"
                },
                config: {
                    Authenticator: {
                        enable_auth_state: false,
                    },
                    ServerApp: {
                        shutdown_no_activity_timeout: 21600
                    }
                },
                resources: {
                    requests: {
                        cpu: 0.1,
                        memory: "512Mi"
                    }
                }
            },
            debug: {
                enabled: true,
            },
            cull: {
                enabled: true,
                users: true,
                removeNamedServers: true,
                timeout: 22000,
                every: 300,
                maxAge: 2160000,
            }
        },
        controller: {
            config: {
                images: {
                    source: {
                        type: "docker",
                        registry: "registry.hub.docker.com",
                        repository: "olaurino/sciplat-lab"
                    }
                },
                lab: {
                    nodeSelector: {
                        nodeType: "jupyter"
                    },
                    tolerations: [
                        {
                            key: "hub.jupyter.org/dedicated",
                            operator: "Equal",
                            value: "user",
                            effect: "NoSchedule",
                        }
                    ],
                    env: {
                        HUB_ROUTE: "/nb/hub",
                        AUTO_REPO_URLS: "https://github.com/juramaga/CSC2_tutorials",
                        AUTO_REPO_BRANCH: "main",
                        AUTO_REPO_SPECS: "https://github.com/juramaga/CSC2_tutorials@main",
                    },
                    initContainers: [{
                        name: "inithome",
                        image: {
                            repository: "ghcr.io/lsst-sqre/nublado-inithome",
                            tag: "4.0.1",
                        },
                        privileged: true,
                        volumeMounts: [
                            {volumeName: "home", containerPath: "/home"},
                        ]
                    }],
                    pullSecret: "pull-secret",
                    volumeMounts: [
                        {volumeName: "home", containerPath: "/home"},
                    ],
                    volumes: [
                        args.cluster.getFileSystem("homes").getStorage("home")
                    ],
                }
            },
        }
    };

    return new PhalanxComponent("cfa-nexus:Nublado", name, values, opts);
}
