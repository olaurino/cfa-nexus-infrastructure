import * as pulumi from "@pulumi/pulumi";
import {PhalanxComponent} from "../phalanx";

export function createPostgresComponent(
    args: {},
    opts?: pulumi.ComponentResourceOptions,
    name?: string
): PhalanxComponent {
    name = name || "postgres"
    const config = new pulumi.Config()
        const postgresConfig = config.requireObject<{ storageClass: string }>(name);
    const storageClass = postgresConfig.storageClass;
    const secretsPath = config.requireObject<{ path: string }>("vaultSecrets").path;
    const values = {
        postgresStorageClass: storageClass,
        global: {
            vaultSecretsPath: secretsPath
        }
    }

    return new PhalanxComponent("cfa-nexus:Postgres", name, values, opts);
}
