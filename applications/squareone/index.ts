import * as pulumi from "@pulumi/pulumi";
import {PhalanxComponent} from "../phalanx";

export function createSquareOneComponent(
    args: {},
    opts?: pulumi.ComponentResourceOptions,
    name?: string
): PhalanxComponent {
    name = name || "squareone"
    const config = new pulumi.Config();
    const fqdn = config.require("fqdn")
    // const soConfig = config.requireObject<{ storageClass: string, clientId: string }>(name)

    const secretsPath = config.requireObject<{ path: string }>("vaultSecrets").path
    const values = {
        global: {
            host: fqdn,
            baseUrl: `https://${fqdn}`,
            vaultSecretsPath: secretsPath
        },
        config: {
            volume_mounts: [
                {name: "var", mountPath: "/var/run"},
                {name: "etc", mountPath: "/etc/nginx"}
            ],
            volumes: [
                {name: "var", emptyDir: {}},
                {name: "etc", emptyDir: {}}
            ],
            image: {
                repository: "cfa-nexus/squareone",
                tag: "latest"
            }
        }
    }

    return new PhalanxComponent("cfa-nexus:SquareOne", name, values, opts);
}
