import * as pulumi from "@pulumi/pulumi";
import {createPostgresComponent} from "./postgres";

import {PhalanxComponent} from "./phalanx"
import {createGafaelfawrComponent} from "./gafaelfawr";
import {createNubladoComponent} from "./nublado";
import {createSquareOneComponent} from "./squareone";
import {createPortalComponent} from "./portal";

type FactoryFunction = (
    args: any,
    opts?: pulumi.ComponentResourceOptions,
    name?: string
) => PhalanxComponent;

interface ComponentTypeMap {
    [key: string]: FactoryFunction;
}

class PhalanxComponentFactory {
    private readonly componentTypeMap: ComponentTypeMap;

    readonly summary: {[k: string]: any} = {}

    constructor() {
        this.componentTypeMap = {
            postgres: createPostgresComponent,
            gafaelfawr: createGafaelfawrComponent,
            nublado: createNubladoComponent,
            squareone: createSquareOneComponent,
            portal: createPortalComponent,
        };
    }

    createComponent(
        type: keyof ComponentTypeMap,
        args: unknown,
        opts?: pulumi.ComponentResourceOptions,
        name?: string
    ): PhalanxComponent {
        const factoryFunction = this.componentTypeMap[type];
        if (!factoryFunction) {
            throw new Error(`Invalid component type: ${type}`);
        }
        const component = factoryFunction(args, opts, name);
        this.summary[component.summary.name] = component.summary
        return component
    }
}

export const applicationFactory = new PhalanxComponentFactory();