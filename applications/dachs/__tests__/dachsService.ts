import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"
import DachsService from ".."

import {describe, it} from "@jest/globals"

describe("DachsService Component", function () {
    beforeAll(async () => {
        // Mocking Pulumi runtime
        await pulumi.runtime.setMocks({
            newResource: function (args: pulumi.runtime.MockResourceArgs): { id: string, state: any } {
                return {
                    id: args.inputs.name + "_id",
                    state: args.inputs, // Mocking the state to simply reflect the input arguments
                };
            },
            call: function (args: pulumi.runtime.MockCallArgs) {
                return args.inputs; // Mocking call to just return provided inputs
            }
        }, "cfa-nexus", "gcp-dev", false);
    })
    it("should create a ConfigMap with the correct data", done => {
        const config = {
            name: "A Name",
            authority: "some.auth",
            port: 9000,
            baseURL: "http://base-url"
        }

        const service = new DachsService("test-dachs", config, {provider: new k8s.Provider("provided", {kubeconfig: ""})});

        pulumi.all([service.configMap]).apply(([data]: any) => {
            expect(data["gavo.rc"]).toBe(`
[web]
sitename: A Name
bindAddress:
serverPort: 9000
serverURL: http://base-url:9000
logFormat: combined
[ivoa]
authority: some.auth
`)
            done()
        })
    })

})
