import * as pulumi from "@pulumi/pulumi";
import * as k8s from "@pulumi/kubernetes";

export interface DachsConfig {
    name: string
    authority: string
    port: number
    baseURL: string
    image: string
}

export class DachsService extends pulumi.ComponentResource {
    public readonly configMap
    public readonly namespace
    private readonly spec
    // private readonly service

    constructor(name: string, config?: Partial<DachsConfig>, opts?: pulumi.ComponentResourceOptions) {
        super("cfa:nexus:DachsService", name, {}, opts);

        const dachsServiceName = config?.name || "CfA Nexus Dachs Service"
        const authority = config?.authority || "nexus.cfa"
        const serverPort = config?.port || 8080
        const baseURL = config?.baseURL || "http://dachs-service.dachs.svc.cluster.local"
        const serverURL = `${baseURL}:${serverPort}`
        const image = config?.image || "olaurino/dachs:2.9.3-4"

        const dachsIni = pulumi.all([dachsServiceName, authority]).apply(([name, auth]: [string, string]) => `
[web]
sitename: ${name}
bindAddress:
serverPort: ${serverPort}
serverURL: ${serverURL}
logFormat: combined
[ivoa]
authority: ${auth}
`);

        const namespace = new k8s.core.v1.Namespace("dachs-namespace", {
            metadata: {name: "dachs"},
        }, {parent: this, provider: opts?.provider})

        const addOpts = {parent: this, provider: opts?.provider, dependsOn: [namespace]};

        const configMap = new k8s.core.v1.ConfigMap("dachs-config", {
            metadata: {
                name: `${name}-config`,
                namespace: "dachs",
            },
            data: {
                "gavo.rc": dachsIni
            },
        }, addOpts);

        const appLabels = {app: `${name}-service`};

        const deployment = new k8s.apps.v1.Deployment(`${name}-deployment`, {
                metadata: {
                    namespace: "dachs",
                    labels: appLabels
                },
                spec: {
                    selector: {matchLabels: appLabels},
                    replicas: 1,
                    template: {
                        metadata: {labels: appLabels},
                        spec: {
                            containers: [{
                                name: "dachs",
                                image,
                                ports: [{containerPort: serverPort}],
                                command: ["/var/gavo/inputs/data/init.sh"],
                                volumeMounts: [{
                                    name: "config-volume",
                                    mountPath: "/etc/gavo.rc",
                                    subPath: "gavo.rc"
                                }],
                                readinessProbe: {
                                    httpGet: {
                                        path: "/tap",
                                        port: serverPort,
                                    },
                                    initialDelaySeconds: 30,
                                    periodSeconds: 10,
                                },
                            }],
                            volumes: [{
                                name: "config-volume",
                                configMap: {
                                    name: configMap.metadata.name,
                                },
                            }],
                        },
                    },
                }
            },
            addOpts
        )

        const service = new k8s.core.v1.Service(`${name}-service`, {
            metadata: {
                name: `${name}`,
                namespace: "dachs",
            },
            spec: {
                selector: appLabels,
                ports: [{
                    port: 8080,
                    targetPort: serverPort
                }],
                type: "ClusterIP" // Default type, only accessible within the cluster
            }
        }, addOpts)

        this.spec = deployment.spec.template.spec
        this.configMap = configMap.data
        this.namespace = namespace.metadata.name
    }
}

export default DachsService;
