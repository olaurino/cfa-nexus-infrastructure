import * as pulumi from "@pulumi/pulumi"
import {Cluster, ClusterFactory} from "./cluster";
import {ApplicationPlatform} from "./app-platform";
import {applicationFactory} from "./applications";
import {ProviderType} from "./cluster/factory";
import {Jupyter} from "./applications/jupyter";
import {StaticWebsite} from "./staticWebsite";
import DachsService, {DachsConfig} from "./applications/dachs";

const config = new pulumi.Config()

const options = {
    name: config.require<ProviderType>("cluster"),
    providerSpecificOptions: {engineVersion: config.get<string>("engineVersion"),}
}

const cluster: Cluster = ClusterFactory.createCluster(options.name, options)

const appPlatform = new ApplicationPlatform("app-platform",
    cluster,
    {dependsOn: cluster, provider: cluster.provider});

const postgres = applicationFactory.createComponent(
    "postgres", {},
    {dependsOn: [appPlatform.argoCd], provider: cluster.provider})

const gafaelfawr = applicationFactory.createComponent(
    "gafaelfawr", {},
    {dependsOn: [postgres], provider: cluster.provider}
)

const jupyter = new Jupyter("jupyter", {
    cluster, ingress: appPlatform.ingressNginx
}, {dependsOn: gafaelfawr, provider: cluster.provider})

const home = new StaticWebsite("home", {provider: cluster.provider,
    dependsOn: [cluster, appPlatform.ingressNginx, gafaelfawr]})

const portal = applicationFactory.createComponent(
    "portal", {},
    {dependsOn: [appPlatform.argoCd], provider: cluster.provider}
)

export const storage = cluster.getFileSystem("homes").getStorage("home");
//
// type NoIpConfig = {
//     username: string, password: string
// }
//
// const noIp = config.requireObject<NoIpConfig>("noIp")
// const hostname = config.require("fqdn")
//
// const dns = cluster.reservedIP.value.apply(async ip => {
//     return new SimpleResource("dev-dns-record", {
//         ip,
//         password: noIp.password,
//         username: noIp.username,
//         hostname
//     }, {dependsOn: cluster.reservedIP.resource})
// })

// export const host = dns.id
export const networkName = cluster.network.value
export const phalanxApplications = applicationFactory.summary

const dachsConfig = config.getObject<DachsConfig>("dachs")
export const dachs = new DachsService("dachs-service", dachsConfig, {provider: cluster.provider})