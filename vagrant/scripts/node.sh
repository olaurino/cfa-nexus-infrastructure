wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq &&\
    chmod +x /usr/bin/yq
token=$(yq '.token' /vagrant/scripts/token.yaml)
microk8s join 192.168.56.10:25000/"${token}" --worker