snap install helm --classic
microk8s.status --wait-ready
microk8s.enable community
microk8s.enable dns
microk8s.enable registry helm3
microk8s.enable "metallb:$1"
curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
sudo install -m 555 argocd-linux-amd64 /usr/local/bin/argocd
rm argocd-linux-amd64
apt install npm
npm install -g n
n lts
curl -fsSL https://get.pulumi.com | sh
mkdir -p ~/.kube
kubectl config view --flatten > ~/.kube/config
microk8s.add-node -l 1200 --format yaml | tee /vagrant/scripts/token.yaml
